#include<bits/stdc++.h>

static int count_index = 1;

namespace glhaqiu
{
  static bool firstRun = true;
}

inline void printLn() {
  if(glhaqiu::firstRun == true)
  {
    std::cout << ">[" << count_index << "]>> [  ]";
    count_index++;
  }
  else
    glhaqiu::firstRun = true;
    
  std::cout << "\n";
}

template<typename T, typename... Ts>
void printLn(const T& arg, const Ts&... args)
{
  if(glhaqiu::firstRun)
    {
      glhaqiu::firstRun = false;
      std::cout << ">[" << count_index << "]>> ";
      count_index++;
    }
  std::cout << arg << " ";
  printLn(args...);
}

template<typename T>
void printLn(const T& arg)
{
  if(glhaqiu::firstRun)
    {
      std::cout << ">[" << count_index << "]>> " << arg << "\n";
      count_index++;
      return;
    }

  std::cout << arg;
}

template<typename T, size_t N>
int ArraySize(const T (&array) [N])
{
  return N;
}
 
template<typename T>
void showTypeSize()
{
  printLn(sizeof(T));
}

#define Show_TypeSize(T) ShowTypeSize<T>()